package network.api.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jin on 11/8/2016.
 */
public class GiphyImage {
    @SerializedName("fixed_height_downsampled")
    public ImageObj highQuality;
    @SerializedName("fixed_width_small")
    public ImageObj smallSize;

}
