package network.api.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jin on 11/8/2016.
 */
public class GiphyImageItem {
    public String type;
    @SerializedName("source_tld")
    public String sourceID;
    public GiphyImage images;
}
