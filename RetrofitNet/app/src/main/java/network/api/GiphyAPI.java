package network.api;

import network.api.bean.GiphyData;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by jin on 11/8/2016.
 */
public interface GiphyAPI {
    String API_KEY = "dc6zaTOxFJmzC";
    int PAGE_SIZE=10;

    @GET("v1/gifs/search?")
    Observable<GiphyData> getGiphyData(
            @Query("q") String keyWord,
            @Query("api_key") String apiKey,
            @Query("limit") int number,
            @Query("offset") int page
    );
}
