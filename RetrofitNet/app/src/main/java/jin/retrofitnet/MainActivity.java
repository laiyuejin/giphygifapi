package jin.retrofitnet;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import adapter.GankAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;
import network.api.GiphyAPI;
import network.api.bean.GiphyImageItem;
import presenter.MainPresenter;
import views.MainView;

public class MainActivity extends AppCompatActivity implements MainView {
    public final static String DEFAULT_KEY_WORD = "funny";
    private static final int PRELOAD_SIZE = 10;
    @Bind(R.id.recycle)
    RecyclerView mRecyclerView;
    @Bind(R.id.refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.btn_search)
    Button btnSearch;
    @Bind(R.id.edit_key_word)
    EditText editKeyWord;
    private View view;
    private StaggeredGridLayoutManager mLayoutManager;
    private GankAdapter adapter;
    private Context context;
    private MainPresenter presenter;
    private String currentKeyWord;
    private boolean mIsLoadMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.context = this;
        presenter = new MainPresenter(this);
        adapter = new GankAdapter(this, mRecyclerView);
        initView();
        presenter.loadData(true, getKeyWord());
    }

    @Override
    protected void onResume() {
        super.onResume();
        editKeyWord.setFocusable(false);
        closeKeybord(editKeyWord, this);
    }

    private String getKeyWord() {
        if (currentKeyWord == null || currentKeyWord.equals(""))
            currentKeyWord = DEFAULT_KEY_WORD;
        return currentKeyWord;
    }

    private void initView() {
        setupSwipeRefresh();
        //init recycleView
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnScrollListener(OnLoadMoreListener());

        //button
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key = editKeyWord.getText().toString();
                if (key == null || key.equals(""))
                    Snackbar.make(mRecyclerView, "please input something!", Snackbar.LENGTH_LONG).show();
                currentKeyWord = key;
                presenter.loadData(true, key);
                editKeyWord.clearFocus();
                closeKeybord(editKeyWord, getBaseContext());
            }
        });
        editKeyWord.setFocusable(false);
        editKeyWord.setFocusableInTouchMode(true);
        editKeyWord.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                editKeyWord.setFocusable(true);
                editKeyWord.setFocusableInTouchMode(true);
                editKeyWord.requestFocus();
                return false;
            }
        });
    }


    @Override
    public void updateData(List<GiphyImageItem> data, boolean clean) {
        adapter.updateItems(data, clean);
        mIsLoadMore = false;
    }

    void setupSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                         @Override
                                                         public void onRefresh() {
                                                             presenter.loadData(true, getKeyWord());
                                                         }
                                                     }
            );
        }
    }

    RecyclerView.OnScrollListener OnLoadMoreListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom = mLayoutManager.findLastCompletelyVisibleItemPositions(
                        new int[2])[1] >= adapter.getItemCount() - GiphyAPI.PAGE_SIZE;
                if (!mSwipeRefreshLayout.isRefreshing() && isBottom) {
                    if (!mIsLoadMore) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        presenter.loadData(false, getKeyWord());
                    } else {
                        mIsLoadMore = false;
                    }
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    public static void closeKeybord(EditText mEditText, Context mContext) {
        mEditText.clearFocus();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
    }
    @Override
    public void setRefreshing(boolean v) {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(v);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void loadError(Throwable throwable) {
        throwable.printStackTrace();
        setRefreshing(false);
        System.out.println(throwable.toString());
        Snackbar.make(mRecyclerView, "load error", Snackbar.LENGTH_LONG).show();
    }
}
