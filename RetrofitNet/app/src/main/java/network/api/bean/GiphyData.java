package network.api.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jin on 11/8/2016.
 */
public class GiphyData {
    public List<GiphyImageItem> data;

    public GiphyData(){
        data = new ArrayList<GiphyImageItem>();
    }
}
