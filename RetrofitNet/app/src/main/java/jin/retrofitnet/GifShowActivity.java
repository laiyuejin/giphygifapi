package jin.retrofitnet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.socks.library.KLog;

import adapter.AbsRecyclerViewAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

public class GifShowActivity extends AppCompatActivity {
    public static final String IMAGE_URL = "image_url";

    @Bind(R.id.gif_show_refresh_layout)
    SwipeRefreshLayout refreshLayout;
    @Bind(R.id.gif_show_image)
    ImageView imageView;
    private String imageURL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_show);
        ButterKnife.bind(this);
        parseIntent();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(false);
            }
        });
        refreshLayout.setRefreshing(true);
        Glide.with(this)
                .load(imageURL)
                .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .crossFade()
                .placeholder(R.drawable.placeholder_image)
                .into(imageView).getSize(new SizeReadyCallback() {
            @Override
            public void onSizeReady(int width, int height) {
                refreshLayout.setRefreshing(false);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public static void startGifImageIntent(Activity context, String url, AbsRecyclerViewAdapter.ClickableViewHolder holder) {
        Intent intent = new Intent(context, GifShowActivity.class);
        intent.putExtra(IMAGE_URL, url);
        context.startActivity(intent);
    }

    private void parseIntent() {
        imageURL = getIntent().getStringExtra(IMAGE_URL);
        KLog.e("imageURL:" + imageURL);
    }
}
