package network.api;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import network.api.bean.GiphyData;
import network.api.bean.GiphyImageItem;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import util.ACache;

/**
 * Created by jin on 11/8/2016.
 */
public class GetGrphyData {
    private static GetGrphyData instance;
    private int dataSource;
//    private String keyWord;

//    public void setKeyWord(String key) {
//        this.keyWord = key;
//    }

    private GetGrphyData() {

    }

    public static GetGrphyData getInstance() {
        if (instance == null) {
            instance = new GetGrphyData();

        }
        return instance;
    }

    public void loadFromNetwork(final Context context, final int page, final BehaviorSubject<List<GiphyImageItem>> behaviorSubject, final String keyWord) {
//        setKeyWord("funny+cat");
        Network.getGiphyAPI(Network.Giphy_URL).getGiphyData(keyWord,GiphyAPI.API_KEY, 10, GiphyAPI.PAGE_SIZE * page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<GiphyData, List<GiphyImageItem>>() {
                         @Override
                         public List<GiphyImageItem> call(GiphyData result) {
                             if (result.data == null) {
                                 return new ArrayList<GiphyImageItem>();
                             }
                             return result.data;
                         }
                     }
                ).doOnNext(new Action1<List<GiphyImageItem>>() {
            @Override
            public void call(List<GiphyImageItem> data) { //load on network
                //change the image to bigger one
                if (data == null) {
                    KLog.e("数据为空");
                    return;
                }
                ACache cache = ACache.get(context);
                String json = new Gson().toJson(data);
                cache.put("sciences_news" + page + keyWord, json, 100);   //save to cache
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<GiphyImageItem>>() {
            @Override
            public void call(List<GiphyImageItem> data) {
                behaviorSubject.onNext(data);
                behaviorSubject.onCompleted();
            }
        });
    }


    public Subscription subscribeData(@NonNull Observer<List<GiphyImageItem>> observer, final Context context, final int page, final String keyWord) {
        final BehaviorSubject<List<GiphyImageItem>> behaviorSubject = BehaviorSubject.create();
        behaviorSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
        Subscription subscription = Observable.create(new Observable.OnSubscribe<List<GiphyImageItem>>() {
            @Override
            public void call(Subscriber<? super List<GiphyImageItem>> subscriber) {
                // first load on the Acache
                String json = ACache.get(context).getAsString("GiphyGif" + page + keyWord);
                if (json == null) { //load one network
                    System.out.println("load on network");
                    loadFromNetwork(context, page, behaviorSubject, keyWord);
                } else {//load on cache
                    List<GiphyImageItem> data = new Gson().fromJson(json, new TypeToken<List<GiphyImageItem>>() {
                    }.getType());
                    System.out.println("load on cache");
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                }
            }
        })
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(behaviorSubject);
        return subscription;
    }
}
