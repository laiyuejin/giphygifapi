package presenter;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import network.api.GetGrphyData;
import network.api.bean.GiphyImageItem;
import rx.Observer;
import rx.Subscription;
import views.MainView;

/**
 * Created by jin on 11/8/2016.
 */
public class MainPresenter {
    public final static String DEFAULT_KEY_WORD = "funny";
    private int mPage = -1;
    private View view;
    private String currentKeyWord;
    private boolean isLoading = false;
    MainView mainView;
    private Subscription subscription;
    public Context context;

    public MainPresenter(MainView view) {
        this.mainView = view;
        this.context = view.getContext();
    }

    public void loadData(final boolean clean, String keyWord) {
        if (isLoading) {
            mainView.setRefreshing(false);
            return;
        }
        if (clean) {
            mPage = -1;
        }
        isLoading = true;
        mPage += 1;

        subscription = GetGrphyData.getInstance().subscribeData(new Observer<List<GiphyImageItem>>() {
            @Override
            public void onCompleted() {
                mainView.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {
                mainView.setRefreshing(false);
                mainView.loadError(e);
            }

            @Override
            public void onNext(List<GiphyImageItem> data) {
                if (data == null) {
                    Toast.makeText(context, "no more data", Toast.LENGTH_LONG).show();
                    return;
                }
                mainView.updateData(data, clean);
                isLoading = false;
            }
        }, context, mPage, keyWord);
    }

//    private String getKeyWord() {
//        if (currentKeyWord == null || currentKeyWord.equals(""))
//            currentKeyWord = DEFAULT_KEY_WORD;
//        return currentKeyWord;
//    }
}
