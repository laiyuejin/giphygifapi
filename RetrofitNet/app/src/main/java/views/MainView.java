package views;

import android.content.Context;

import java.util.List;

import network.api.bean.GiphyImageItem;

/**
 * Created by jin on 11/8/2016.
 */
public interface MainView {
    void setRefreshing(boolean b);
    Context getContext();
    void loadError(Throwable throwable);
    void updateData(List<GiphyImageItem> data,boolean clean);
}
