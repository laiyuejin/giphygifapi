package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.List;

import customview.RatioImageView;
import jin.retrofitnet.GifShowActivity;
import jin.retrofitnet.R;
import network.api.bean.GiphyImageItem;
import network.api.bean.ImageObj;


public class GankAdapter extends AbsRecyclerViewAdapter {
    public List<GiphyImageItem> datas = new ArrayList<>();
    private Context context;

    public GankAdapter(final Activity activity, RecyclerView recyclerView) {
        super(recyclerView);
        setOnItemClickListener(new AbsRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, AbsRecyclerViewAdapter.ClickableViewHolder holder) {
                String bigURL = datas.get(position).images.highQuality.url;
                GifShowActivity.startGifImageIntent(activity, bigURL,holder);

            }
        });
    }

    public void updateItems(List<GiphyImageItem> addData,boolean clean) {
        if(clean)
            datas.clear();
//        animateItems = animated;
//        lastAnimatedPosition = -1;
        datas.addAll(addData);
        if(clean){
            notifyDataSetChanged();
        }else{
            int size = datas.size();
            notifyItemRangeInserted(size-addData.size(),size);
        }
    }

    @Override
    public ClickableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        bindContext(parent.getContext());
        return new ItemViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.card_item_gank, parent, false));
    }

    @Override
    public void onBindViewHolder(ClickableViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.mTextView.setText("from:" + datas.get(position).sourceID);
            ImageObj image = datas.get(position).images.smallSize;

            String url = image.url;
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)  itemViewHolder.imageView.getLayoutParams();
//            params.width = image.width / 2;
            params.height = image.height;
            itemViewHolder.imageView.setLayoutParams(params);


            itemViewHolder.imageView.setOriginalSize(image.width,image.height);
//            itemViewHolder.imageView.set
            KLog.e(image.height);
            Glide.with(getContext())
                    .load(url).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.placeholder_image)
                    .into(itemViewHolder.imageView);
        }
        super.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public class ItemViewHolder extends AbsRecyclerViewAdapter.ClickableViewHolder {
        public RatioImageView imageView;
        public TextView mTextView;
        public View item;

        public ItemViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            imageView = $(R.id.item_img);
            mTextView = $(R.id.item_tv);
        }
    }
}
